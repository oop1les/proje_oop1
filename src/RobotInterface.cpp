#include "RobotInterface.h"

/**
* @file RobotInterface.cpp
* @author ÖMER SENTURK 152120161084<br>
* @date 25.01.2021
*/
RobotInterface::RobotInterface() {

}

void RobotInterface::setPos(Pos* posi) {
	pos = posi;
}

void RobotInterface::setSensor(RangeSensor* sensor) {
	this->rangeSensor = sensor;
}

RobotInterface::~RobotInterface() {

}