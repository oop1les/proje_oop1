#include "RobotControl.h"

/**
* @file RobotControl.cpp
* @author ÖMER SENTURK 152120161084<br>
* @date 25.01.2021
*/
RobotControl::RobotControl() {

}

RobotControl::RobotControl(RobotInterface* robotInterface, Pos* posi, RangeSensor* sensor) {
	this->sensor.push_back(sensor);
	this->robotInterface = robotInterface;
	this->pos = posi;
	this->robotInterface->setPos(pos);
	this->robotInterface->setSensor(this->sensor.at(0));
}

void RobotControl::Move(int speed) {
	robotInterface->move(speed);
}

void RobotControl::forward() {
	robotInterface->turn(RobotInterface::DIR::forward);
}

void RobotControl::turnRight() {
	robotInterface->turn(RobotInterface::DIR::right);
}

void RobotControl::turnLeft() {
	robotInterface->turn(RobotInterface::DIR::left);
}

void RobotControl::moveDistance(float distance) {
	robotInterface->move(distance);
	Sleep(distance);
	robotInterface->stop();
}

void RobotControl::closeWall() {
	bool control_conflict = false;

	for (int sensCounter = 0; sensCounter < sensor.size(); sensCounter++) {
		robotInterface->update();
		if ((int)sensor.at(sensCounter)->getRange(3) < 100 || sensor.at(sensCounter)->getRange(4) < 100) {
			robotInterface->stop();
			control_conflict = true;
			break;
		}
	};

	while (control_conflict == false) {
		robotInterface->move(100);
		for (int sensCounter = 0; sensCounter < sensor.size(); sensCounter++) {
			if ((int)sensor.at(sensCounter)->getRange(3) < 300 || (int)sensor.at(sensCounter)->getRange(4) < 300) {
				robotInterface->stop();
				control_conflict = true;
				break;
			}
		}
	}
}

void RobotControl::print() {
	cout << "Position is (" << pos->getX() << "," << pos->getY() << "," << pos->getZ() << ")" << endl;
	cout << "Sonar ranges are [ ";
	for (int counter = 0; counter < sensor.size(); counter++) {
		for (int i = 0; i < 16; i++) {
			cout << sensor.at(counter)->getRange(i) << " ";
		}
		cout << "]" << endl;
	}
}

RobotControl::~RobotControl() {

}