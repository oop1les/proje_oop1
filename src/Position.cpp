#include "Position.h"

/**
* @file Position.cpp
* @author KISMET AKTAS 152120191088<br>
* @date 25.01.2021
*/
Pos::Pos() {
	this->x = 0;
	this->y = 0;
	this->z = 0;
}

float Pos::getX() {
	return this->x;
}

float Pos::getY() {
	return this->y;
}

float Pos::getZ() {
	return this->z;
}

void Pos::setX(float x) {
	this->x = x;
}

void Pos::setY(float y) {
	this->y = y;
}

void Pos::setZ(float z) {
	this->z = z;
}

bool Pos::operator==(const Pos& pos) {
	if (this->getX() != pos.x)
		return false;
	if (this->getY() != pos.y)
		return false;
	if (this->getZ() != pos.z)
		return false;
	return true;
}

Pos::~Pos() {

}