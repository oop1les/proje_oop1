#pragma once
#include "RangeSensor.h"

/**
* @file SonarSensor.h
* @author �MER SENTURK 152120161084<br>
* @date 25.01.2021
*/
class SonarSensor : public RangeSensor {
private:
	float uRange[16];
	string type;
public:
	SonarSensor();
	float getRange(int index);
	void setRange(float range[]);
	float getMin();
	float getMax();
	void updateSensor(float uRange[]);
	float operator[](int op_i);
	~SonarSensor();
};