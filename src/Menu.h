#pragma once
#include "RobotInterface.h"
#include "RobotControl.h"
#include <iostream>

/**
* @file Menu.h
* @author MERT BULUT 152120181101<br>
* @date 25.01.2021
*/
class Menu
{
private:
	Pos *pos;
	RangeSensor *sensor;
	RobotInterface *robotInterface;
	RobotControl *robotControl;
	bool connectionStatus;
public:
	
	Menu();
	
	int cInput(int mRange, string input);
	void mainMenu();
	void connectionMenu();
	void sensorMenu();
	void motionMenu();
	float speed();
	int moveSpeed();
	float Distance();
	int cInteger(string input);
	float cFloat(string input);
	~Menu();
};
