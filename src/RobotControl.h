#pragma once
#include "RobotInterface.h"
#include "RangeSensor.h"
#include "SonarSensor.h"
#include "Position.h"
#include "PioneerRobotInterface.h"
#include <vector>
#include <math.h>

/**
* @file RobotControl.h
* @author �MER SENTURK 152120161084<br>
* @date 25.01.2021
*/
using namespace std;

class RobotControl {
private:
	vector<RangeSensor *> sensor;
	Pos *pos;
	RobotInterface *robotInterface;
public:
	RobotControl();
	RobotControl(RobotInterface *robotInterface, Pos *pos, RangeSensor * sensor);
	void Move(int speed);
	void forward();
	void turnRight();
	void turnLeft();
	void moveDistance(float distance);
	void closeWall();
	void print();
	~RobotControl();
};
