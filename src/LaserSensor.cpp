#include "LaserSensor.h"

/**
* @file LaserSensor.cpp
* @author KISMET AKTAS 152120191088<br>
* @date 25.01.2021
*/

LaserSensor::LaserSensor() {

}

float LaserSensor::getRange(int index) {
	return 0;
}

void LaserSensor::setRange(float range[]) {

}

void LaserSensor::updateSensor(float uRange[]) {

}

float LaserSensor::operator[](int op_i) {
	return 0.0f;
}

float LaserSensor::getMin() {
	return this->min;
}

float LaserSensor::getMax() {
	return this->max;
}

LaserSensor::~LaserSensor() {
}