#pragma once
#include "RobotInterface.h"
#include "PioneerRobotAPI.h"
#include "SonarSensor.h"

/**
* @file PioneerRobotInterface.h
* @author MERT BULUT 152120181101<br>
* @date 25.01.2021
*/
class PioneerRobotInterface : public RobotInterface, public PioneerRobotAPI {
private:
	RangeSensor *rangeSensor;
	Pos *pos;
public:
	PioneerRobotInterface();
	PioneerRobotInterface(RangeSensor *sensor, Pos *pos);
	void update();
	bool open();
	void move(float speed);
	bool close();
	void turn(RobotInterface::DIR dir);
	void stop();
	void updateRobot();
	~PioneerRobotInterface();
};
