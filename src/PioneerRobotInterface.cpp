#include "PioneerRobotInterface.h"

/**
* @file PioneerRobotInterface.cpp
* @author MERT BULUT 152120181101<br>
* @date 25.01.2021
*/
PioneerRobotInterface::PioneerRobotInterface() {
	setRobot(this);
}

PioneerRobotInterface::PioneerRobotInterface(RangeSensor* sensor, Pos* pos) {
	setRobot(this);
	this->rangeSensor = sensor;
	this->pos = pos;
}


void PioneerRobotInterface::update() {
	float ranges[16];
	float min, max;
	pos->setX(getX());
	pos->setY(getY());
	pos->setZ(getZ());
	setPos(pos);
	getSonarRange(ranges);
	rangeSensor->updateSensor(ranges);
	setSensor(rangeSensor);
}

bool PioneerRobotInterface::open() {
	bool status = connect();
	if (status == 1)
		update();

	return status;

}

void PioneerRobotInterface::move(float speed) {
	moveRobot(speed);
}

bool PioneerRobotInterface::close()
{
	return disconnect();
}

void PioneerRobotInterface::turn(RobotInterface::DIR dir) {
	switch (dir) {
	case RobotInterface::DIR::forward:
		turnRobot(PioneerRobotAPI::DIR::forward);
		break;
	case RobotInterface::DIR::right:
		turnRobot(PioneerRobotAPI::DIR::right);
		break;
	case RobotInterface::DIR::left:
		turnRobot(PioneerRobotAPI::DIR::left);
		break;
	default:
		break;
	}
}

void PioneerRobotInterface::stop() {
	stopRobot();
}

void PioneerRobotInterface::updateRobot() {
	pos->setX(getX());
	pos->setY(getY());
	pos->setZ(getZ());
	setPos(pos);
	float ranges[16];
	float min, max;
	getSonarRange(ranges);
	rangeSensor->updateSensor(ranges);
	setSensor(rangeSensor);
}


PioneerRobotInterface::~PioneerRobotInterface()
{

}