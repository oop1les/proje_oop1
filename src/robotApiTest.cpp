#include "PioneerRobotAPI.h"
#include "PioneerRobotInterface.h"
#include "RobotControl.h"
#include <iostream>
#include "Menu.h"

/**
* @file robotApiTest.cpp
* @author MERT BULUT 152120181101<br>
* @date 25.01.2021
*/
using namespace std;

int main() {
	Menu* menu = new Menu();
	RobotControl* robotControl;
	RobotInterface* robotInterface;
	robotControl->Move(10000);
	robotControl->turnLeft();
	Sleep(1000);
	robotControl->print();

	robotControl->forward();
	Sleep(1000);
	robotControl->print();

	robotControl->turnRight();
	Sleep(1000);
	robotInterface->stop();
	cout << "Press any key to exit...";
	getchar();
	robotInterface->close();
}