#pragma once
#include "Position.h"
#include "RangeSensor.h"

/**
* @file RobotInterface.h
* @author �MER SENTURK 152120161084<br>
* @date 25.01.2021
*/
class RobotInterface {
private:
	Pos *pos;
	RangeSensor *rangeSensor;
public:
	enum DIR {
		left = -1,
		forward = 0,
		right = 1
	};
	RobotInterface();
	void setPos(Pos *pos);
	void setSensor(RangeSensor *sensor);
	virtual bool open() = 0;
	virtual bool close() = 0;
	virtual void update() = 0;
	virtual void move(float speed) = 0;
	virtual void turn(DIR dir) = 0;
	virtual void stop() = 0;
	~RobotInterface();
};
