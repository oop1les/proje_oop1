#pragma once
#include "RangeSensor.h"

/**
* @file LaserSensor.h
* @author KISMET AKTAS 152120191088<br>
* @date 25.01.2021
*/

class LaserSensor : public RangeSensor
{
private:
	float min, max;
	string type;
public:
	LaserSensor();
	float getRange(int index);
	void setRange(float range[]);
	void updateSensor(float uRange[]);
	float getMin();
	float getMax();
	float operator[](int op_i);
	~LaserSensor();

};

