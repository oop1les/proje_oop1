#include "SonarSensor.h"

/**
* @file SonarSensor.cpp
* @author ÖMER SENTURK 152120161084<br>
* @date 25.01.2021
*/
SonarSensor::SonarSensor() {
	this->type = "Sonar L131 Sensor";
	setType("Sonar L131 Sensor");
}

float SonarSensor::getRange(int index) {
	return this->uRange[index];
}

void SonarSensor::setRange(float range[]) {
	for (int i = 0; i < 16; i++) {
		this->uRange[i] = range[i];
	}
}

float SonarSensor::getMin() {
	float minValue = this->uRange[0];
	for (int i = 1; i < 16; i++) {
		if (minValue > this->uRange[i]) {
			minValue = this->uRange[i];
		}
	}
	return minValue;
}

float SonarSensor::getMax() {
	float maxValue = this->uRange[0];
	for (int i = 1; i < 16; i++) {
		if (maxValue < this->uRange[i]) {
			maxValue = this->uRange[i];
		}
	}
	return maxValue;
}

void SonarSensor::updateSensor(float uRange[]) {
	for (int i = 0; i < 16; i++) {
		this->uRange[i] = uRange[i];
	}
}

float SonarSensor::operator[](int i) {
	return this->uRange[i];
}

SonarSensor::~SonarSensor() {

}