#pragma once
#include <string>

/**
* @file RangeSensor.h
* @author KISMET AKTAS 152120191088<br>
* @date 25.01.2021
*/
using namespace std;

class RangeSensor {
private:
	string type;
public:
	RangeSensor();
	virtual void updateSensor(float uRange[]) = 0;
	virtual float getMin() = 0;
	virtual float getMax() = 0;
	virtual float getRange(int index) = 0;
	void setType(string type);
	string getType();
	virtual float operator[](int op_i) = 0;
	~RangeSensor();
};