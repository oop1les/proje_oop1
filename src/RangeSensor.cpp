#include "RangeSensor.h"

/**
* @file RangeSensor.cpp
* @author KISMET AKTAS 152120191088<br>
* @date 25.01.2021
*/
RangeSensor::RangeSensor() {

}

void RangeSensor::setType(string type) {
	this->type = type;
}

string RangeSensor::getType() {
	return this->type;
}

RangeSensor::~RangeSensor() {

}