#pragma once

/**
* @file Position.h
* @author KISMET AKTAS 152120191088<br>
* @date 25.01.2021
*/
class Pos {
private:
	float x, y, z;
public:
	Pos();
	float getX();
	float getY();
	float getZ();
	void setX(float x);
	void setY(float y);
	void setZ(float z);
	bool operator==(const Pos& pos);
	~Pos();
};
